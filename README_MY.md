# Desafio Angular

> 1. Implementar recurso para CRUD de histórico de preço de combustível
> 2. Implementar interface que retorne a média de preço de combustível com base no nome do município
> 3. Implementar interface que retorne os dados agrupados por distribuidora
> 4. Implementar interface que retorne o valor médio do valor da compra e do valor da venda por município

#### Item concluidos

{1}

## Planejamento

Dillinger requires [Node.js](https://nodejs.org/) v10+ to run.

Eu tive ao todo 4 dias para implentar todos esses recursos pedido, e meu horário livre para implentar esses recursos era de 5h por dia, meu planejamento inicial era desenvolver a parte do CRUD nos 2 primeiros dias, e as interfaces nos outros dois.

## Problemas

#### Dia 01
horas trabalhadas: 5h

Não consegui realizar o commit inicial para o repositório do git lab Por conta de ter criado o projeto antes de puxar do git lab, demorei uma 1h para resolver o problema.

Nesse dia, conseguir implentar até a criação de um novo registro no historico de preços de combustivel, faltando a leitura, atualização e o delete.


## Dia 02
horas trabalhadas: 5h

Esse dia foi o momento em que me deparei com o CORS quando fui implementar a atualização, esse foi um problema que não consegui resolver nesse mesmo dia. e perdi um tempo valioso que me fez atrasar a entrega dos outros modulos.

Resumindo perdi minhas 5h de trabalho pra resolver um problema que nunca tinha me deparado antes.

## Dia 03

horas trabalhadas: 5h

Foi o dia em que eu consegui finalmente resolver o problema de CORS, através da implementação de proxy local .js que era ativado por uma flag.

Acredito que não tenho sido a melhor alternativa, mas foi o que eu tentei fazer para contornar o erro.

Nesse mesmo dia conclui o CRUD mais no delete ficou dando um erro da hora de apagar o registro, o metodo funciona mais não retorna pra página inicial, e o navegador mostra um erro sem especificações. Não consegui resolver.

## Dia 04

horas trabalhadas: 4h

Foi o dia que eu realizei ajustes e adapatações para tornar o layout responsivo, já que vi que não seria possível concluir os outros 3 modulos da aplicação.

Resultado em vídeo do que foi prduzido:

https://drive.google.com/file/d/1zDp2YPjSbGA6aSd0qKSA5R4Kk6XuOrb2/view?usp=sharing

#### ponderações

Queria ter tido mais tempo até pra escrever isso, não qual estou concluindo agora: 11:58h.