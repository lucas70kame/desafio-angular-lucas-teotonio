import { HeaderService } from 'src/app/componentes/template/header/header.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-distribuidora',
  templateUrl: './distribuidora.component.html',
  styleUrls: ['./distribuidora.component.css']
})
export class DistribuidoraComponent implements OnInit {

  constructor(private headerService: HeaderService) { 
    headerService.headerData = {
      title: 'Dados Agrupados por Distribuidora',
      icon: 'local_shipping',
      routeUrl: ''
    }
  }

  ngOnInit(): void {
  }

}
