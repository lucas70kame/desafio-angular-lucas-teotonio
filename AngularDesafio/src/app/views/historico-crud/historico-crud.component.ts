import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { HeaderService } from 'src/app/componentes/template/header/header.service';

@Component({
  selector: 'app-historico-crud',
  templateUrl: './historico-crud.component.html',
  styleUrls: ['./historico-crud.component.css']
})
export class HistoricoCrudComponent implements OnInit {

  constructor(private router: Router, private headerService: HeaderService) { 
    headerService.headerData = {
      title: 'Histórico Preço Combustível',
      icon: 'history',
      routeUrl: ''
    }
  }

  ngOnInit(): void {
  }

  navegarNovoRegistro(): void{
    this.router.navigate(['/criar']);
  }

}
