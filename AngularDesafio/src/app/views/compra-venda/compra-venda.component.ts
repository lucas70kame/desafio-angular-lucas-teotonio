import { HeaderService } from 'src/app/componentes/template/header/header.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-compra-venda',
  templateUrl: './compra-venda.component.html',
  styleUrls: ['./compra-venda.component.css']
})
export class CompraVendaComponent implements OnInit {

  constructor(private headerService: HeaderService) { 
    headerService.headerData = {
      title: 'Valor Médio Compra e Venda',
      icon: 'inventory',
      routeUrl: ''
    }
  }

  ngOnInit(): void {
  }

}
