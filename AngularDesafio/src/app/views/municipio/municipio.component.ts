import { HeaderService } from 'src/app/componentes/template/header/header.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-municipio',
  templateUrl: './municipio.component.html',
  styleUrls: ['./municipio.component.css']
})
export class MunicipioComponent implements OnInit {

  constructor(private headerService: HeaderService) { 
    headerService.headerData = {
      title: 'Media Preço por Municipio',
      icon: 'room',
      routeUrl: ''
    }
  }

  ngOnInit(): void {
  }
  

}
