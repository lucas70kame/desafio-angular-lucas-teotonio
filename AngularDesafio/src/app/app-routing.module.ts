import { MunicipioComponent } from './views/municipio/municipio.component';
import { CompraVendaComponent } from './views/compra-venda/compra-venda.component';
import { DistribuidoraComponent } from './views/distribuidora/distribuidora.component';
import { HistoricoDeleteComponent } from './componentes/historico/historico-delete/historico-delete.component';
import { HistoricoUpdateComponent } from './componentes/historico/historico-update/historico-update.component';
import { HistoricoCreateComponent } from './componentes/historico/historico-create/historico-create.component';
import { HistoricoCrudComponent } from './views/historico-crud/historico-crud.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: "",
    component: HistoricoCrudComponent
  },
  {
    path: "criar",
    component: HistoricoCreateComponent
  },
  {
    path: "atualizar/:id",
    component: HistoricoUpdateComponent
  },
  {
    path: "deletar/:id",
    component: HistoricoDeleteComponent
  },
  {
    path: "distribuidora",
    component: DistribuidoraComponent
  },
  {
    path: "compra-venda",
    component: CompraVendaComponent
  },
  {
    path: "municipio",
    component: MunicipioComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
