import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HeaderComponent } from './componentes/template/header/header.component';
import { NavComponent } from './componentes/template/nav/nav.component';
import { FooterComponent } from './componentes/template/footer/footer.component';

import { HistoricoCrudComponent } from './views/historico-crud/historico-crud.component';

import {MatToolbarModule} from '@angular/material/toolbar';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatCardModule} from '@angular/material/card';
import {MatListModule} from '@angular/material/list';

import {MatButtonModule} from '@angular/material/button';
import { HistoricoReadComponent } from './componentes/historico/historico-read/historico-read.component';
import { HistoricoCreateComponent } from './componentes/historico/historico-create/historico-create.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';

import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms'
import {MatFormFieldModule} from '@angular/material/form-field'
import {MatInputModule} from '@angular/material/input';
import { HistoricoUpdateComponent } from './componentes/historico/historico-update/historico-update.component';
import { HistoricoDeleteComponent } from './componentes/historico/historico-delete/historico-delete.component';

import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { DistribuidoraComponent } from './views/distribuidora/distribuidora.component';
import { DistribuidoraReadComponent } from './componentes/distribuidora/distribuidora-read/distribuidora-read.component';
import { MunicipioComponent } from './views/municipio/municipio.component';
import { CompraVendaComponent } from './views/compra-venda/compra-venda.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    NavComponent,
    FooterComponent,
    HistoricoCrudComponent,
    HistoricoReadComponent,
    HistoricoCreateComponent,
    HistoricoUpdateComponent,
    HistoricoDeleteComponent,
    DistribuidoraComponent,
    DistribuidoraReadComponent,
    MunicipioComponent,
    CompraVendaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatSidenavModule,
    MatCardModule,
    MatListModule,
    MatButtonModule,
    MatSnackBarModule,
    HttpClientModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
