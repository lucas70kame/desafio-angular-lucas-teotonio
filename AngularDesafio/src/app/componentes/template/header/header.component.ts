import { HeaderService } from './header.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private headerService: HeaderService) { }

  ngOnInit(): void {
  }

  icon_bars: string = "menu";

  ativarMenu(): void{

    if(this.icon_bars == "menu"){
      this.icon_bars = "close";
    } 
    else if(this.icon_bars == "close"){
      this.icon_bars = "menu";
    }

    const container = document.querySelector('.container');
    container.classList.toggle("active");
  }

  get title(): string{
    return this.headerService.headerData.title;
  }

  get icon(): string{
    return this.headerService.headerData.icon;
  }

  get routeUrl(): string{
    return this.headerService.headerData.routeUrl;
  }

}
