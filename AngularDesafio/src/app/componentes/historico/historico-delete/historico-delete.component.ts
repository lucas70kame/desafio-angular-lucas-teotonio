import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { Historico } from '../model/historico.model';
import { HistoricoService } from './../servicos/historico.service';

@Component({
  selector: 'app-historico-delete',
  templateUrl: './historico-delete.component.html',
  styleUrls: ['./historico-delete.component.css']
})
export class HistoricoDeleteComponent implements OnInit {

  historico: Historico = {
    id:0,
    combustivel:'',
    data: '',
    preco: null
  };

  constructor(private router: Router, 
              private HistoricoService: HistoricoService,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.HistoricoService.readById(id).subscribe(historico => {
      this.historico = historico;
    });
  }

  deleteHistorico(): void {
    this.HistoricoService.delete(this.historico.id).subscribe(() => {
      this.HistoricoService.showMessage('Registro excluído com sucesso!');
      this.router.navigate(['']);
    });
  }

  cancelar(): void {
    this.router.navigate(['']);
  }

}
