import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { Historico } from '../model/historico.model';
import { HistoricoService } from './../servicos/historico.service';

@Component({
  selector: 'app-historico-read',
  templateUrl: './historico-read.component.html',
  styleUrls: ['./historico-read.component.css']
})
export class HistoricoReadComponent implements OnInit {

  historico: Historico[] = [];

  displayedColumns = ['id', 'combustivel','data', 'preco','action'];

  constructor(private HistoricoService: HistoricoService) { }

  ngOnInit(): void {
    this.HistoricoService.read().subscribe(historico => {
      this.historico = historico;
      console.log(historico);
    })
  }
  

}
