import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { Historico } from '../model/historico.model';
import { HistoricoService } from './../servicos/historico.service';

@Component({
  selector: 'app-historico-update',
  templateUrl: './historico-update.component.html',
  styleUrls: ['./historico-update.component.css']
})
export class HistoricoUpdateComponent implements OnInit {

  historico: Historico = {
    id:null,
    combustivel:'',
    data: '',
    preco: null
  };

  constructor(private router: Router, 
              private HistoricoService: HistoricoService,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.HistoricoService.readById(id).subscribe(historico => {
      this.historico = historico;
    });
  }

  AtualizarRegistro(){
    this.HistoricoService.update(this.historico).subscribe(() => {
      this.HistoricoService.showMessage('Registro Atualizado com sucesso!');
      this.router.navigate(['']);
    })
  }

  coverterData(): void{
    var data = this.historico.data.split('/').reverse().join('-');
    this.historico.data = data;
  }

  cancelar(): void {
    this.router.navigate(['']);
  }
  

}
