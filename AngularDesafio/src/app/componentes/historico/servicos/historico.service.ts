import { Injectable } from '@angular/core';
import { MatSnackBar } from'@angular/material/snack-bar';
import { Historico } from '../model/historico.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EMPTY, Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HistoricoService {

  
  baseUrl = '/api';

  constructor(private snackBar: MatSnackBar, private http: HttpClient) { }


  showMessage(msg: string, isError: boolean = false):void{
    this.snackBar.open(msg,'X',{
      duration: 3000,
      horizontalPosition: 'right',
      verticalPosition: 'top',
      panelClass: isError ? ['msg-error'] : ['msg-success']
    })
  }

  errorHandle(e: any): Observable<any>{
    console.log(e)
    this.showMessage('Ocorreu um erro!',true);
    return EMPTY;
  }

  read(): Observable<Historico[]>{
    return this.http.get<Historico[]>(this.baseUrl).pipe(
      map(obj => obj),
      catchError(e => this.errorHandle(e))
    );
  }

  create(historico: Historico): Observable<Historico> {
    return this.http.post<Historico>(this.baseUrl, historico);
  }

  update(historico: Historico): Observable<Historico>{
    return this.http.put<Historico>(this.baseUrl, historico);
  }

  readById(id: string): Observable<Historico>{
    const url = `${this.baseUrl}/${id}`;
    return this.http.get<Historico>(url);
  }

  delete(id: number): Observable<Historico>{
    const url = `${this.baseUrl}/${id}`;
    return this.http.delete<Historico>(url);
  }

}




