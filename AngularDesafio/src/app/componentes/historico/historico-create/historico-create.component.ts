import { HistoricoService } from './../servicos/historico.service';
import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { Historico } from '../model/historico.model';

@Component({
  selector: 'app-historico-create',
  templateUrl: './historico-create.component.html',
  styleUrls: ['./historico-create.component.css']
})
export class HistoricoCreateComponent implements OnInit {


  
  historico: Historico = {
    combustivel:'',
    data: '',
    preco: null
  }
  
  
  constructor(private router: Router, private HistoricoService: HistoricoService) { }

  ngOnInit(): void {
  }

  cancelar(): void {
    this.router.navigate(['']);
  }

  criarRegistro(): void {
    this.coverterData();
    this.HistoricoService.create(this.historico).subscribe(() => {
      this.HistoricoService.showMessage('Novo Registro Adicionado!');
      this.router.navigate(['']);
    });
    
  }

  coverterData(): void{
    var data = this.historico.data.split('-').reverse().join('/');
    this.historico.data = data;
  }

}
