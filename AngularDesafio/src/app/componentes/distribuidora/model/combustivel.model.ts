export interface Combustivel{
    id?: number,
    bandeira:string,
    codInstalacao:number,
    dataColeta:	string,
    municipio:	string,
    produto: string,
    revendedora:string,
    siglaEstado:string,
    siglaRegiao:string,
    unidadeMedida:string,
    valorCompra: number,
    valorVenda:	number
}