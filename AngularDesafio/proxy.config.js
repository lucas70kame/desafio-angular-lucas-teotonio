const PROXY_CONFIG = [
    {
      context: '/api',
      target: 'https://combustivelapp.herokuapp.com/api/historico/',
      secure: false,
      logLevel: 'debug',
      pathRewrite: {'^/api' : ''},
      "changeOrigin": true,
      "headers": {
        "Server":"Cowboy",
        "Connection": "keep-alive",
        "Vary":"Origin",
        "Vary":"Access-Control-Request-Method",
        "Vary":"Access-Control-Request-Headers",
        "Content-Type": "application/json",
        "responseType": 'text'
        // "Transfer-Encoding": "chunked",
        // "normalizedNames":{},
        // "lazyUpdate":null
      },
    }
  ];
  module.exports = PROXY_CONFIG;